import pandas as pd
import mysql.connector
from sqlalchemy import types, create_engine

# MySQL Connection
MYSQL_USER = 'admim'
MYSQL_PASSWORD = 'program1'
MYSQL_HOST_IP = 'myfirstmysql.c7i7klvtkifp.ap-south-1.rds.amazonaws.com'
MYSQL_PORT = '3306'
MYSQL_DATABASE = 'mydb'

engine = create_engine(
    'mysql+mysqlconnector://' + MYSQL_USER + ':' + MYSQL_PASSWORD + '@' + MYSQL_HOST_IP + ':' + MYSQL_PORT + '/' + MYSQL_DATABASE,
    echo=False)

chunksize = 25

idx = 1
for df in pd.read_csv("runningshoe_men_amazon_puma.csv", chunksize=chunksize):
    if idx == 1:
        exists = 'replace'
    else:
        exists = 'append'
    df.to_sql(name='PumaProduct', con=engine, if_exists='append', index=False, chunksize=chunksize)
    print(str(chunksize * idx) + " Processed")
    idx = idx + 1