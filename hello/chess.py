class blank:
    name = "Blank"


class Chess:
    def __init__(self):
        self.board = [[blank()]*8 for _ in range(8)]

    def initialize(self):
        for i in range(8):
            self.board[1][i] = pawn("W", [1, i])

        for i in range(8):
            self.board[6][i] = pawn("B", [6, i])

    def play_game(self):
        while True:

            x = int(input("enter item to move : x : ")) - 1
            y = int(input("enter item to move : y : ")) - 1
            if self.board[x][y].name != "Blank":
                x_new = int(input("enter new position : x : ")) - 1
                y_new = int(input("enter new position : y : ")) - 1
              
                if self.board[x][y].isvalid_move(x_new,y_new):
                    print("good")
                    return False
                else:
                    print("fail")

    def print_board(self):
        for x in range(8):
            for y in range(8):
                print(self.board[x][y].name, end=" ")
            print(" ")


class pawn(Chess):
    mov_path_x = [1,1]
    mov_path_y = [-1,1]
    name = "Pawn"

    def __init__(self,player,position):
        self.player = player
        self.position = position
        Chess.__init__(self)

    def isvalid_move(self, x_new, y_new):
        """for i in range(len(self.mov_path_x)):
            if self.player == 'W':
                x = self.position[0] + self.mov_path_x[i]
                y = self.position[1] + self.mov_path_y[i]
                if x_new == x and y_new == y:
                    if self.board[x_new][y_new].name == blank.name:
                        return False
                    else:
                        print("player removed the opponents ",self.board[x_new][y_new].name)
                        del self.board[x_new][y_new]
                        self.position = [x_new,y_new]
                        self.board[x_new][y_new] = self
                        return True"""
        if x_new == self.position[0] + 1 and y_new == self.position[0]:
            if self.board[x_new][y_new].name == blank.name:

                self.board[x_new][y_new] ,self.board[self.position[0]][self.position[1]] = self.board[self.position[0]][self.position[1]], self.board[x_new][y_new]
                self.position = [x_new, y_new]
                return True
        return False


chess1 = Chess()
chess1.initialize()
chess1.print_board()
chess1.play_game()
chess1.print_board()



