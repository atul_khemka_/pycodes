class Node:
    def __init__(self,key):
        self.key = key
        self.left = self.right = None


def height(root):
    if root is None:
        return 0
    return 1 + max(root.left, root.right)


def identical(x,y):
    if x is None and y is None:
        return True
    return x and y and (x.key == y.key) and identical(x.left, y.left) and identical(x.right, y.right)


def mirror(root):
    if root is None:
        return
    mirror(root.left)
    mirror(root.right)
    root.right,root.left = root.left,root.right


def inorder(root):
    if root:
        inorder(root.left)
        print(root.key, end=" ")
        inorder(root.right)


if __name__ == "__main__":
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.left.right = Node(5)
    root.right.left = Node(6)
    root.right.right = Node(7)
    inorder(root)
    mirror(root)
    print('\n')
    inorder(root)

