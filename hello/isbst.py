class Node:

    def __init__(self, val):
        self.data = val
        self.left = self.right = None


def isbst(node):
    return isbstutil(node, None, None)


# can use int_min int_max instead of l r
def isbstutil(node, l, r):
    if node is None:
        return True

    if l and node.data <= l.data:
        return False
    if r and node.data >= r.data:
        return False
    return isbstutil(node.left, l, node) and isbstutil(node.right, node, r)


def isBST(node):
    return isBSTutil(node, prev=None)


def isBSTutil(node, prev):
    if node:
        if isBSTutil(node.left, prev) is False:
            return False
            # important to note the return false as required to move to right subtree
        if prev and prev.data >= node.data:
            return False

        prev = node
        return isBSTutil(node.right, prev)

    return True


if __name__ == '__main__': 
    root = Node(3)
    root.left = Node(2)  
    root.right = Node(5)  
    root.right.left = Node(1)  
    root.right.right = Node(4)
    """ root = Node(4)
    root.left = Node(2)
    root.right = Node(5)
    root.left.left = Node(1)
    root.left.right = Node(3) """
    # root.right.left.left = Node(40)
    if isBST(root):
        print("Is BST") 
    else: 
        print("Not a BST")
