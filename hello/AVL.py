class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None
        self.height = 1


class AVLTree:

    def insert(self, root, key):
        if not root:
            return TreeNode(key)
        elif key < root.val:
            root.left = self.insert(root.left, key)
        else:
            root.right = self.insert(root.right, key)

        root.height = 1 + max(self.get_height(root.left), self.get_height(root.right))

        bal = self.get_balance(root)

        # case 1 LL imbalance
        if bal > 1 and key < root.left.val:
            return self.rotate_right(root)
        # case 2 RR imbalance
        if bal < -1 and key > root.right.val:
            return self.rotate_left(root)
        # case 3 LR imbalance
        if bal > 1 and key > root.left.val:
            root.left = self.rotate_left(root.left)
            return self.rotate_right(root)
        # case 4 RL imbalance
        if bal < -1 and key < root.right.val:
            root.right = self.rotate_right(root.right)
            return self.rotate_left(root)
        return root

    def rotate_right(self, node):

        lc = node.left
        lc_r = lc.right

        lc.right = node
        node.left = lc_r

        node.height = 1 + max(self.get_height(node.left), self.get_height(node.right))
        lc.height = 1 + max(self.get_height(lc.left), self.get_height(lc.right))
        return lc

    def rotate_left(self, node):
        rc = node.right
        rc_l = rc.left

        rc.left = node
        node.right = rc_l

        node.height = 1 + max(self.get_height(node.left), self.get_height(node.right))
        rc.height = 1 + max(self.get_height(rc.left), self.get_height(rc.right))
        return rc

    def get_balance(self, node):
        if not node:
            return 0
        else:
            return self.get_height(node.left) - self.get_height(node.right)

    def preorder(self, node):
        if node:
            print(node.val, end=" ")
            self.preorder(node.left)
            self.preorder(node.right)

    def get_height(self, node):
        if not node:
            return 0
        else:
            return node.height

    def delete_node(self, root, key):
        if not root:
            return root
        if key < root.val:
            root.left = self.delete_node(root.left, key)
        elif key > root.val:
            root.right = self.delete_node(root.right, key)
        else:
            if root.left is None:
                temp = root.right
                root = None
                return temp
            if root.right is None:
                temp = root.left
                root = None
                return temp
            succ_parent = root.right
            temp = root.right
            while temp.left:
                succ_parent = temp
                temp = temp.left
            succ_parent.left = temp.right
            root.data = temp.data
            del temp

        if root is None:
            return root
        root.height = 1 + max(self.get_height(root.left), self.get_height(root.right))

        bal = self.get_balance(root)

        if bal > 1 and self.get_balance(root.left) >= 0:
            return self.rotate_right(root)
        if bal < -1 and self.get_balance(root.right) <= 0:
            return self.rotate_left(root)
        if bal > 1 and self.get_balance(root.left) < 0:
            root.left = self.rotate_left(root.left)
            return self.rotate_right(root)
        if bal < -1 and self.get_balance(root.right) >0:
            root.right = self.rotate_right(root.right)
            return self.rotate_left(root)

        return root


myTree = AVLTree()
root = None

root = myTree.insert(root, 10)
root = myTree.insert(root, 20)
root = myTree.insert(root, 30)
root = myTree.insert(root, 40)
root = myTree.insert(root, 50)
root = myTree.insert(root, 25)

myTree.preorder(root)