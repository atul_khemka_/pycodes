def reverse(s):
    print(s)
    if len(s) == 1:
        return s
    else:
        return reverse(s[1:]) + s[0]


s = "atul"

print("The original string  is : ")
print(s)

print("The reversed string(using recursion) is : ")
print(reverse(s))