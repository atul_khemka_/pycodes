def convert(arr, n):
    temp = [arr[i] for i in range(n)]
    temp.sort()
    umap = {}
    val = 0
    for i in range(n):
        umap[temp[i]] = val
        val += 1
    for i in range(n):
        arr[i] = umap[arr[i]]


def printarr(arr, n):
    for i in range(n):
        print(arr[i], end=" ")


if __name__ =="__main__":
    arr = [10, 20, 15, 12, 11, 50]
    n = len(arr)
    print("original array :")
    printarr(arr, n)
    convert(arr, n)
    print("reduced array :")
    printarr(arr, n)
    
