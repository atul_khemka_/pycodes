import re
board = ["-"]*9
game_still_going = True
current_player = 'X'
winner = None


def display_board():
    print(board[0] + ' | ' + board[1] + ' | ' + board[2])
    print(board[3] + ' | ' + board[4] + ' | ' + board[5])
    print(board[6] + ' | ' + board[7] + ' | ' + board[8])


def play_game():
    display_board()
    while game_still_going:
        handle_turn(current_player)
        if check_game_over():
            break
        flip_player()

    if winner is None:
        print("It's a Tie!!")
    else:
        print(f"player {winner} is winner ")


def handle_turn(player):
    print(f"it is player {player} turn ")
    position = input("Enter position between 1-9 ")
    pattern = r'\b\d\b'
    valid = False
    while not valid:
        while not re.search(pattern, position) or position == '0':
            position = input("Enter position between 1-9 ")

        position = int(position) - 1

        if board[position] == "-":
            valid = True
        else:
            print("position is filled , Enter another position")
            position = str(position)

    board[position] = player
    display_board()


def flip_player():
    global current_player
    if current_player == 'X':
        current_player = "O"
    else:
        current_player = 'X'


def check_game_over():
    if check_win():
        return True
    elif check_tie():
        return True
    return False


def check_win():
    if check_row() or check_column() or check_diagonal():
        return True
    else:
        return False


def check_row():
    global game_still_going
    global winner
    row_1 = board[0] == board[1] == board[2] != "-"
    row_2 = board[3] == board[4] == board[5] != "-"
    row_3 = board[6] == board[7] == board[8] != "-"
    if row_1 or row_2 or row_3:
        game_still_going = False

    if row_1:
        winner = board[0]
        return True
    elif row_2:
        winner = board[3]
        return True
    elif row_3:
        winner = board[6]
        return True
    return False


def check_column():
    global game_still_going
    global winner
    row_1 = board[0] == board[3] == board[6] != "-"
    row_2 = board[1] == board[4] == board[7] != "-"
    row_3 = board[2] == board[5] == board[8] != "-"
    if row_1 or row_2 or row_3:
        game_still_going = False

    if row_1:
        winner = board[0]
        return True
    elif row_2:
        winner = board[1]
        return True
    elif row_3:
        winner = board[2]
        return True
    return False


def check_diagonal():
    global game_still_going
    global winner
    dig_1 = board[0] == board[4] == board[8] != "-"
    dig_2 = board[2] == board[4] == board[6] != "-"

    if dig_1 or dig_2:
        game_still_going = False

    if dig_1:
        winner = board[0]
        return True
    elif dig_2:
        winner = board[2]
        return True
    return False


def check_tie():
    global game_still_going
    if "-" not in board:
        game_still_going = False
        return True
    return False


play_game()

