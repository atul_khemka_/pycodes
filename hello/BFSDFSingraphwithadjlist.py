from collections import defaultdict


class Graph:

    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)

    def BFS(self, s):
        visited = [False] * (len(self.graph))
        q = [s]
        visited[s] = True
        while q:
            s = q.pop(0)
            print(s, end=" ")
            for i in self.graph[s]:
                if not visited[i]:
                    q.append(i)
                    visited[i] = True

    def DFS(self, s):
        visited = [False] * (len(self.graph))
        self.DFSutil(s, visited)
        """ for a disconnected graph visit every vertex
        for i in range(length): 
            if visited[i] == False: 
                self.DFSUtil(i, visited) """

    def DFSutil(self, s, visited):
        visited[s] = True
        print(s, end=" ")
        for i in self.graph[s]:
            if not visited[i]:
                self.DFSutil(i, visited)

    def mother(self):
        n = len(self.graph)
        visited = [False] * n
        v = 0
        for i in range(n):
            if not visited[i]:
                self.DFSutil(i, visited)
                v = i
        # idea is last finished vertex in DFS is mother
        visited = [False] * n
        self.DFSutil(v, visited)
        if any(not i for i in visited):
            return -1
        else:
            return v


g = Graph()
g.add_edge(0, 1)
g.add_edge(0, 2)
g.add_edge(1, 2)
g.add_edge(2, 0)
g.add_edge(2, 3)
g.add_edge(3, 3)

print("Following is Breadth First Traversal (starting from vertex 2)")
g.BFS(2)
print("\nFollowing is Depth First Traversal (starting from vertex 2)")
g.DFS(2)
print("\nA mother vertex is ", g.mother())
