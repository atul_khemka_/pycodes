def isIsomorphic(s: str, t: str):
    if len(s) != len(t):
        return False
    map1 = {}
    for i in range(len(s)):
        if s[i] in map1.keys():
            if map1[s[i]] != t[i]:
                return False
        else:
            if t[i] in map1.values():
                return False
            map1[s[i]] = t[i]

    print(map1)
    return True


s = "egg"
t = "add"
print(isIsomorphic(s, t))
