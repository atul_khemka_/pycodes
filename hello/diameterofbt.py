class Node:

    def __init__(self, key):
        self.data = key
        self.left = None
        self.right = None


def max_diameter(root):
    get_diameter.dia = 0
    get_diameter(root)
    return get_diameter.dia


def get_diameter(root):

    if not root:
        return 0

    left_height = get_diameter(root.left)
    right_height = get_diameter(root.right)

    curr_dia = left_height + right_height + 1

    if curr_dia > get_diameter.dia:
        get_diameter.dia = curr_dia

    return max(left_height, right_height) + 1


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.right = Node(4)
root.right.left = Node(5)
root.right.right = Node(6)
root.right.left.left = Node(7)
root.right.left.right = Node(8)

print(max_diameter(root))
