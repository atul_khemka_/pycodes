def noofcombi(arr, m, s):
    table = [0]*(s+1)
    table[0] = 1
    for i in range(m):
        for j in range(arr[i], s+1):
            table[j] = table[j] + table[j-arr[i]]
    return table[s]


def min_coins(arr, m, s):
    table = [float("Inf")] * (s + 1)
    table[0] = 0
    for i in range(m):
        for j in range(arr[i], s + 1):
            table[j] = min(table[j], 1 + table[j - arr[i]])
    return table[s]


arr = [1,2,5]
s = 11
print(noofcombi(arr, len(arr), s))
print(min_coins(arr, len(arr), s))