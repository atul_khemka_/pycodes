# A Python program for Prim's Minimum Spanning Tree (MST) algorithm
class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = [([0]*self.V) for i in range(self.V)]

    def getminkey(self, mstset, key):
        mini = float("Inf")
        for i in range(self.V):
            if not mstset[i]:
                if key[i] != float("Inf") and key[i] < mini:
                    mini = key[i]
                    min_index = i
        return min_index

    def primMST(self):
        key = [float("Inf")]*self.V
        parent = [None]*self.V
        mstset = [False]*self.V
        key[0] = 0
        parent[0] = -1
        for x in range(self.V):
            u = self.getminkey(mstset, key)
            mstset[u] = True
            for y in range(self.V):
                if 0 < self.graph[u][y] < key[y] and not mstset[y]:
                    key[y] = self.graph[u][y]
                    parent[y] = u

        self.printMST(parent)

    def printMST(self, parent):
        print("Edge \tWeight")
        for i in range(1, self.V):
            print(parent[i], "-", i, "\t", self.graph[i][parent[i]])


g = Graph(5)
g.graph = [[0, 2, 0, 6, 0],
           [2, 0, 3, 8, 5],
           [0, 3, 0, 0, 7],
           [6, 8, 0, 0, 9],
           [0, 5, 7, 9, 0]]

g.primMST()