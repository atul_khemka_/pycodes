def combination_sum(arr, summ):
    inter_res = []
    result = []
    # remove duplicate val and sort it
    arr = sorted(list(dict.fromkeys(arr)))
    find_combination(arr, summ, inter_res, result, 0)
    return result


def find_combination(arr, summ, inter_res, result, i):

    if summ < 0:
        return

    if summ == 0:
        print("dnd    ", str(result), inter_res)
        result.append(inter_res)
        print(result)
        return

    while i < len(arr) and summ - arr[i] >= 0:
        inter_res.append(arr[i])
        print(" inter res ", inter_res)
        find_combination(arr, summ - arr[i], inter_res, result, i)
        i += 1
        inter_res.pop()


if __name__ == "__main__":
    arr_input = [2,4,8,6,2]
    summ = 8
    res = combination_sum(arr_input, summ)

    print(res)
