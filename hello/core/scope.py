def scope_test():
    def do_local():
        spam = "local spam"

    def do_nonlocal():
        nonlocal spam  # indicates that particular variables live in an enclosing scope and should be rebound there
        spam = "nonlocal spam"

    def do_global():
        global spam  # indicate that particular variables live in the global scope and should be rebound there
        spam = "global spam"

    spam = "test spam"
    do_local()
    print("After local assignment: ", spam )
    do_nonlocal()
    print("After nonlocal Assignment: ",spam)
    do_global()
    print("After global: ",spam)


scope_test()
print("in global scope:", spam)