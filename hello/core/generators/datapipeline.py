file_name = 'techcrunch.csv'
lines = (line for line in open(file_name))
list_line = (s.rstrip().split(',') for s in lines)
cols = next(list_line)
company_dicts = (dict(zip(cols, info)) for info in list_line)
funding = (int(company_dict['raisedAmt']) for company_dict in company_dicts if company_dict['round'] == 'a')
total_series_a = sum(funding)
print("Total funding raised in series A $", total_series_a)

dromes = ("demigod", "rewire", "madam", "freer", "anutforajaroftuna", "kiosk")

palindromes = filter(lambda word: word == word[::-1], dromes)
mapping = map(lambda w: w.upper(),dromes)
print(mapping.__next__())
print(next(mapping))

