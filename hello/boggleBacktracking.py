# given 2d matrix find all possible strings present in dictionary
rowpath = [0, 1, -1, 1, -1, 0, 1, -1]
colpath = [1, 1, 1, 0, 0, -1, -1, -1]


def find_Word(board, visited, row, col, word, dict1):

    if word in dict1:
        print(word)
    if len(word) == 9:
        return
    for i in range(len(rowpath)):
        rownew = row + rowpath[i]
        colnew = col + colpath[i]
        if isValid(visited, rownew, colnew):
            visited[rownew][colnew] = True
            find_Word(board, visited, rownew, colnew, word + board[rownew][colnew], dict1)
            visited[rownew][colnew] = False


def isValid(visited, rownew, colnew):
    if 0 <= rownew < 3 and 0 <= colnew < 3 and not visited[rownew][colnew]:
        return True
    else:
        return False


dict1 = ["geeks", "for", "go", "quiz"]
board = [['g', 'i', 'z'], ['u', 'e', 'k'], ['q', 's', 'e']]
visited = [[False] * 3 for _ in range(3)]
for i in range(3):
    for j in range(3):
        visited[i][j] = True
        find_Word(board, visited, i, j, board[i][j], dict1)
        visited[i][j] = False
