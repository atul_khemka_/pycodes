def mamsumKsubarray(arr, k):
    n = len(arr)

    if k > n:
        print("K cannot be greater than array")
        return

    window_sum = sum(arr[:k])
    max_sum = window_sum

    for i in range(n-k):
        window_sum = window_sum - arr[i] + arr[i+k]
        max_sum = max(window_sum,max_sum)

    print(max_sum)


if __name__ == "__main__":
    arr = [1, 4, 2, 10, 2, 3, 1, 0, 20]
    k = 4
    mamsumKsubarray(arr,k)


