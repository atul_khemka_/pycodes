rowpath = [1, -1, 0, 0]
colpath = [0, 0, 1, -1]


def findpathinmaze(maze, visited, row, col, dest_row, dest_col, move):
    if row == dest_row and col == dest_col:
        for i in range(4):
            print(visited[i])
        print("////////////////")
    else:
        for i in range(len(rowpath)):
            row_new = row + rowpath[i]
            col_new = col + colpath[i]
            if validmove(maze,visited,row_new,col_new):
                move += 1
                visited[row_new][col_new] = move
                findpathinmaze(maze, visited, row_new, col_new, dest_row, dest_col, move)
                move -= 1
                visited[row_new][col_new] = 0


def validmove(maze, visited, row, col):
    if 0 <= row < 4 and 0 <= col < 4 and maze[row][col] == 1 and visited[row][col] == 0:
        return True
    return False


maze = [[1, 1, 0, 1], [0, 1, 1, 1], [0, 1, 0, 1], [0, 1, 1, 1, ]]
for i in range(4):
    print(maze[i])
print("/////////////")
visited = [[0] * 4 for _ in range(4)]
visited[0][0] = 1
findpathinmaze(maze, visited, 0, 0, 3, 3, 1)
