def findCycle(arr, start_index):
    n = len(arr)
    visited = [False]*n
    count = 0
    flag = 0
    while 0 <= start_index < n:
        if not visited[start_index]:
            visited[start_index] = True
            if start_index == arr[start_index]:
                return -1
            else:
                start_index = arr[start_index]
                count += 1
        else:
            flag = 1
            break

    if flag == 1:
        return count
    else:
        return -1


if __name__ == "__main__":
    arr = [1, 2, 2, 3]
    start_index = 0
    print(findCycle(arr, start_index))