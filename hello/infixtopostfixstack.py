class conversion:
    def __init__(self,capacity):
        self.top = -1
        self.capacity = capacity
        self.arr_stack = []
        self.output = []
        self.precedence = {'+': 1, '-': 1, '*': 2, '/': 2, '^': 3}

    def push(self, op):
        self.top +=1
        self.arr_stack.append(op)

    def pop(self):
        if not self.isEmpty():
            self.top -= 1
            return self.arr_stack.pop()
        else :
            return '$'

    def peek(self):
        return self.arr_stack[-1]

    def isEmpty(self):
        return True if self.top == -1 else False

    def is_operand(self, ch):
        return ch.isalpha()

    def not_greater(self, i):
        try:
            a = self.precedence[i]
            b = self.precedence[self.peek()]
            return True if a <= b else False
        except KeyError:
            return False

    def infixtopostfix(self, exp):
        for i in exp:
            if self.is_operand(i):
                self.output.append(i)
            elif i == '(':
                self.push(i)
            elif i == ')':
                while not self.isEmpty() and self.peek() != '(':
                    a = self.pop()
                    self.output.append(a)

                if not self.isEmpty() and self.peek() != '(':
                    return -1
                else:
                    self.pop()

            else:
                while not self.isEmpty() and self.not_greater(i):
                    self.output.append(self.pop())

                self.push(i)

        while not self.isEmpty():
            self.output.append(self.pop())

        for i in range(len(self.output)):
            print(self.output[i], end= " ")


exp = "a+b*(c^d-e)^(f+g*h)-i"
obj = conversion(len(exp))
obj.infixtopostfix(exp)





