def binary_search(arr,low,high,key):

    if low <= high:
        mid = (low + high)//2
        if arr[mid] == key:
            return mid
        elif key > arr[mid]:
            return binary_search(arr,mid+1, high,key)
        else:
            return binary_search(arr,low,mid-1,key)
    else:
        return -1


if __name__ == '__main__':
    sortedlist = [12, 34, 45, 54, 57, 66, 69, 72]
    size = len(sortedlist)
    key = 100
    print(binary_search(sortedlist, 0, size-1, key))