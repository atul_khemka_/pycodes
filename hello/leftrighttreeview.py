class Node:

    def __init__(self,key):
        self.data = key
        self.left = None
        self.right = None


def printlevelorder(root):

    if root is None:
        return

    queue = [root]
    while len(queue) > 0:
        for i in range(len(queue)):
            if i == 0:
                print(queue[0].data)
            node = queue.pop(0)

            if node.left:
                queue.append(node.left)

            if node.right:
                queue.append(node.right)

def printlevelorder_right(root):

    if root is None:
        return

    queue = [root]
    while len(queue) > 0:
        x = len(queue)
        for i in range(x):

            if i == x - 1:
                print(queue[0].data)
            node = queue.pop(0)

            if node.left:
                queue.append(node.left)

            if node.right:
                queue.append(node.right)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
print("left view of binary tree is - ")
printlevelorder(root)
print("right view of binary tree is -")
printlevelorder_right(root)