INT_MIN = float("-infinity")
INT_MAX = float("infinity")


class Node:

    def __init__(self, val):
        self.data = val
        self.left = self.right = None


def constructTreeUtil(pre, key, mini, maxi, size):
    constructTreeUtil.preindex = getattr(constructTreeUtil, 'preindex', 0)
    if constructTreeUtil.preindex >= size:
        return None
    root = None
    if maxi > key > mini:
        root = Node(key)
        constructTreeUtil.preindex += 1
        if constructTreeUtil.preindex < size:
            root.left = constructTreeUtil(pre, pre[constructTreeUtil.preindex], mini, key, size)
            root.right = constructTreeUtil(pre, pre[constructTreeUtil.preindex], key, maxi, size)
    return root


def constructTree(pre):
    size = len(pre)
    return constructTreeUtil(pre, pre[0], INT_MIN, INT_MAX, size)


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data)
        inorder(root.right)


pre = [10, 5, 1, 7, 40, 50]
root = constructTree(pre)

print("Inorder traversal of the constructed tree: ")
inorder(root)