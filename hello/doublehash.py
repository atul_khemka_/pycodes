table_size = 13
prime = 7


class doublehash:

    def __init__(self):
        self.curr_size = 0
        self.hashtable = [-1]*table_size

    def isfull(self):
        return self.curr_size == table_size

    def hash1(self, key):
        return key % table_size

    def hash2(self, key):
        return prime - (key % prime)

    def inserthash(self, key):

        if self.isfull():
            return
        index = self.hash1(key)
        if self.hashtable[index] != -1:
            index2 = self.hash2(key)
            i = 1
            while 1:
                newindex = (index + i * index2) % table_size

                if self.hashtable[newindex] == -1:
                    self.hashtable[newindex] = key
                    self.curr_size += 1
                    break
                i += 1
        else:
            self.hashtable[index] = key
            self.curr_size += 1

    def displayhash(self):
        for i in range(0 , table_size):
            if self.hashtable[i] != -1:
                print(i, end="")
                print("-->", end="")
                print(self.hashtable[i])
            else:
                print(i)


if __name__ == '__main__':

    a = [19, 27, 36, 10, 64]
    n = len(a)
    hash1 = doublehash()
    for i in range(0, n):
        hash1.inserthash(a[i])

    hash1.displayhash()
