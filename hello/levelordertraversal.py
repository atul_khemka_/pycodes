class Node:

    def __init__(self,key):
        self.data = key
        self.left = None
        self.right = None


def printlevelorder(root):

    if root is None:
        return

    queue = []
    queue.append(root)
    while len(queue) > 0:

        print(queue[0].data)
        node = queue.pop(0)

        if node.left:
            queue.append(node.left)

        if node.right:
            queue.append(node.right)


def height(root):
    if root is None:
        return 0
    else:
        lheight = height(root.left)
        rheight = height(root.right)
        if lheight > rheight:
            return lheight + 1
        else:
            return rheight + 1


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
print(height(root))
print("Level Order Traversal of binary tree is -")
printlevelorder(root)