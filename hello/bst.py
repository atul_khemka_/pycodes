class Node:

    def __init__(self, val):
        self.data = val
        self.left = self.right = None


def insert(root, node):
    if root is None:
        root = node
    else:
        if root.data < node.data:
            if root.right is None:
                root.right = node
            else:
                insert(root.right, node)
        else:
            if root.left is None:
                root.left = node
            else:
                insert(root.left, node)


def search(root, key):
    if root is None or root.data == key:
        return root

    if root.data < key:
        return search(root.right, key)

    else:
        return search(root.left, key)


def inorder(root):
    if root:
        inorder(root.left)
        print(root.data)
        inorder(root.right)


def delete_node(root, key):
    if root is None:
        return root
    if root.data < key:
        root.right = delete_node(root.right, key)
    elif root.data > key:
        root.left = delete_node(root.left, key)
    else:
        if root.left is None:
            temp = root.right
            root = None
            return temp
        if root.right is None:
            temp = root.left
            root = None
            return temp
        succ_parent = root.right
        temp = root.right
        while temp.left:
            succ_parent = temp
            temp = temp.left
        succ_parent.left = temp.right
        root.data = temp.data
        del temp
    return root


r = Node(50)
insert(r, Node(30))
insert(r, Node(20))
insert(r, Node(40))
insert(r, Node(70))
insert(r, Node(60))
insert(r, Node(80))
if search(r, 4):
    print("key present")
else:
    print("not found")

inorder(r)
print("Delete 20")
r = delete_node(r, 20)
print("Inorder traversal of the modified tree")
inorder(r)

print("Delete 30")
r = delete_node(r, 30)
print("Inorder traversal of the modified tree")
inorder(r)

print("Delete 50")
r = delete_node(r, 50)
print("Inorder traversal of the modified tree")
inorder(r)
