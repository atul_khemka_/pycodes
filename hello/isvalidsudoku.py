def isValidConfig(board, n):
    new_set = set()

    for i in range(n):
        for j in range(n):
            if board[i][j] == '.':
                continue
            row = board[i][j] + "row" + str(i)
            column = board[i][j] + "column" + str(j)
            box = board[i][j] + "box" + str(i//3) + "-" + str(j//3)

            if row in new_set:
                return False
            if column in new_set:
                return False
            if box in new_set:
                return False

            new_set.add(row)
            new_set.add(column)
            new_set.add(box)

    return True


if __name__ == "__main__":

    board = [['5', '3', '.', '.', '7', '.', '.', '.', '.'],
             ['6', '.', '.', '1', '9', '5', '.', '.', '.'],
             ['.', '9', '8', '.', '.', '.', '.', '6', '.'],
             ['8', '.', '.', '.', '6', '.', '.', '.', '3'],
             ['4', '.', '.', '8', '.', '3', '.', '.', '1'],
             ['7', '.', '.', '.', '2', '.', '.', '.', '6'],
             ['.', '6', '.', '.', '.', '.', '2', '8', '.'],
             ['.', '.', '.', '4', '1', '9', '.', '.', '5'],
             ['.', '.', '.', '.', '8', '.', '.', '7', '9']]

    if isValidConfig(board, 9):
        print("YES")
    else:
        print("NO")

