class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.g = [([0]*self.V) for i in range(self.V)]

    def minkey(self, dist, sssp):
        mini = float("Inf")
        for i in range(self.V):
            if not sssp[i]:
                if dist[i] != float("Inf") and mini > dist[i]:
                    mini = dist[i]
                    min_index = i
        return min_index

    def Dijkstra(self, src):
        dist = [float("Inf")]*self.V
        sssp = [False]*self.V
        dist[src] = 0

        for x in range(self.V):

            u = self.minkey(dist, sssp)
            sssp[u] = True
            for v in range(self.V):
                if self.g[u][v] > 0 and not sssp[v] and dist[u] + self.g[u][v] < dist[v]:
                    dist[v] = dist[u] + self.g[u][v]

        self.printarr(dist)

    def printarr(self, dist):
        print("Vertex   Distance from Source")
        for i in range(self.V):
            print("% d \t\t % d" % (i, dist[i]))


g = Graph(9)
g.g = [[0, 4, 0, 0, 0, 0, 0, 8, 0],
           [4, 0, 8, 0, 0, 0, 0, 11, 0],
           [0, 8, 0, 7, 0, 4, 0, 0, 2],
           [0, 0, 7, 0, 9, 14, 0, 0, 0],
           [0, 0, 0, 9, 0, 10, 0, 0, 0],
           [0, 0, 4, 14, 10, 0, 2, 0, 0],
           [0, 0, 0, 0, 0, 2, 0, 1, 6],
           [8, 11, 0, 0, 0, 0, 1, 0, 7],
           [0, 0, 2, 0, 0, 0, 6, 7, 0]]

g.Dijkstra(0)

