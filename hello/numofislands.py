def num_islands(arr):
    count = 0
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] == 1:
                count += 1
                BFS(arr, i, j)

    return count


def BFS(arr, i, j):
    if i < 0 or i >= len(arr):
        return
    if j < 0 or j >= len(arr[i]):
        return
    if arr[i][j] == 0:
        return

    arr[i][j] = 0
    BFS(arr, i-1, j)
    BFS(arr, i+1, j)
    BFS(arr, i, j+1)
    BFS(arr, i, j-1)


arr = [[1,1,0,0,0],[1,1,0,0,0],[0,0,1,1,0],[0,0,0,1,1]]
print("no of islands found :", num_islands(arr))
