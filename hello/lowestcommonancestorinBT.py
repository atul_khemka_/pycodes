class Node:

    def __init__(self, key):
        self.data = key
        self.left = None
        self.right = None


def findLCA_util(root, n1, n2, V):
    if not root:
        return None

    if root.data == n1:
        V[0] = True
        return root
    if root.data == n2:
        V[1] = True
        return root

    left_lcs = findLCA_util(root.left, n1, n2, V)
    right_lcs = findLCA_util(root.right, n1, n2, V)

    if left_lcs and right_lcs:
        return root

    return left_lcs if left_lcs else right_lcs


def find(node, key):
    if not node:
        return False
    if node.data == key or find(node.left, key) or find(node.right, key):
        return True
    return False


def findLCA(root, n1, n2):
    V = [False, False]
    lca = findLCA_util(root, n1, n2, V)

    if V[0] and V[1] or V[0] and find(lca, n2) or V[1] and find(lca, n1):
        return lca

    return None


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

lca = findLCA(root, 4, 5)

if lca is not None:
    print("LCA(4, 5) = ", lca.data)
else:
    print("Keys are not present")

lca = findLCA(root, 4, 10)
if lca is not None:
    print("LCA(4,10) = ", lca.data)
else:
    print("Keys are not present")
