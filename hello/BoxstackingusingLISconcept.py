class Box:

    def __init__(self, h, w, d):
        self.h = h
        self.w = w
        self.d = d

    def __lt__(self, other):
        return self.w*self.d < other.w*other.d


def maxboxheight(arr, n):

    rot = [Box(0,0,0) for _ in range(3*n)]
    index = 0
    for i in range(n):
        rot[index].h = arr[i].h
        rot[index].d = max(arr[i].d, arr[i].w)
        rot[index].w = min(arr[i].d, arr[i].w)
        index += 1

        rot[index].h = arr[i].w
        rot[index].d = max(arr[i].d, arr[i].h)
        rot[index].w = min(arr[i].d, arr[i].h)
        index += 1

        rot[index].h = arr[i].d
        rot[index].d = max(arr[i].h, arr[i].w)
        rot[index].w = min(arr[i].h, arr[i].w)
        index += 1

    n *= 3
    rot.sort(reverse=True)
    msh = [0]*n
    for i in range(n):
        msh[i] = rot[i].h

    for i in range(1,n):
        for j in range(0,i):
            if rot[j].w > rot[i].w and rot[j].d > rot[i].d:
                if msh[i] < msh[j] + rot[i].h:
                    msh[i] = msh[j] + rot[i].h

    maxm = -1
    for i in range(n):
        maxm = max(maxm, msh[i])

    return maxm


if __name__ == "__main__":
    arr = [Box(4,6,7), Box(1,2,3), Box(4,5,6), Box(10,12,32)]
    n = len(arr)
    print("max box stack height: ", maxboxheight(arr, n))
