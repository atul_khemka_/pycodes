class node:

    def __init__(self, data):
        self.data = data
        self.next = None


class LL:

    def __init__(self):
        self.head = None

    def push(self, data):
        new_node = node(data)
        new_node.next = self.head
        self.head = new_node

    def delete(self, key):
        temp = self.head
        if temp is not None and temp.data == key:
            self.head = temp.next
            temp = None
            return

        while temp is not None:
            if temp.data == key:
                break
            prev = temp
            temp = temp.next

        if temp is None:
            return

        prev.next = temp.next
        temp = None

    def reverse(self):
        curr = self.head
        prev = None
        while curr:
            nxt = curr.next
            curr.next = prev
            prev = curr
            curr = nxt
        self.head = prev

    def reverse_rec(self):
        if self.head is None:
            return
        self.reverse_rec_util(self.head, None)

    def reverse_rec_util(self, curr, prev):
        if curr.next is None:
            self.head = curr
            curr.next = prev
            return
        nxt = curr.next
        curr.next = prev
        self.reverse_rec_util(nxt, curr)

    def printll(self):
        tmp = self.head
        while tmp:
            print(tmp.data, end=" ")
            tmp = tmp.next

    def get_count(self, node):

        if not node:
            return 0
        else:
            return 1 + self.get_count(node.next)


if __name__ == '__main__':

    l1 = LL()
    l1.head = node(1)
    s2 = node(2)
    s3 = node(3)
    l1.head.next = s2
    s2.next = s3
    l1.push(21)
    # l1.delete(11)
    l1.printll()
    print("\ncount of nodes ",l1.get_count(l1.head))
    #l1.reverse()
    l1.reverse_rec()
    print ("\nafetr reversal")
    l1.printll()

