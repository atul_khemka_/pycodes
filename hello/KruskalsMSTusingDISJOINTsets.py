class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = []

    def add_edge(self, u, v, w):
        self.graph.append([u, v, w])

    def find(self, parent, x):
        if parent[x] != x:
            parent[x] = self.find(parent, parent[x])
        return parent[x]

    def union(self, parent, rank, x, y):
        x_set = self.find(parent, x)
        y_set = self.find(parent, y)

        if rank[x_set] < rank[y_set]:
            parent[x_set] = y_set
            rank[y_set] += 1
        elif rank[y_set] < rank[x_set]:
            parent[y_set] = x_set
            rank[x_set] += 1
        else:
            parent[y_set] = x_set
            rank[x_set] += 1

    def KruskalMST(self):
        parent = [i for i in range(self.V)]
        rank = [0]*self.V
        result = []
        i = 0
        e = 0
        self.graph = sorted(self.graph, key=lambda item: item[2])
        while e < self.V - 1:
            u, v, w = self.graph[i]
            i += 1
            x = self.find(parent, u)
            y = self.find(parent, v)

            if x != y:
                e += 1
                result.append([u, v, w])
                self.union(parent, rank, x, y)

        print("Following are the edges in the constructed MST")
        for u, v, weight in result:
            print("%d -- %d == %d" % (u, v, weight))



g = Graph(4)
g.add_edge(0, 1, 10)
g.add_edge(0, 2, 6)
g.add_edge(0, 3, 5)
g.add_edge(1, 3, 15)
g.add_edge(2, 3, 4)

g.KruskalMST() 