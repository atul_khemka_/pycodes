def printUnsorted(arr,n):
    left, right = 0, n-1
    for left in range(0,n-1):
        if arr[left] > arr[left+1]:
            break

    if left == n-2:
        print("sorted array")
        exit()

    while right > 0:
        if arr[right] < arr[right-1]:
            break
        right -= 1

    mx = max(arr[(left+1):right])
    mn = min(arr[(left+1):right])
    for i in range(0,left):
        if arr[i] > mn:
            left = i
            break

    i = n - 1
    while i >= right + 1:
        if arr[i] < mx:
            right = i
            break
        i -= 1

    print("The unsorted subarray which makes the given array")
    print("sorted lies between the indexes %d and %d" % (left, right))

arr = [10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60]
arr_size = len(arr)
printUnsorted(arr, arr_size)
