def findLenght(arr, n):
    max_len = 1
    for i in range(n-1):
        mx = arr[i]
        mn = arr[i]
        myset = set()
        myset.add(arr[i])
        for j in range(i+1, n):
            if arr[j] in myset:
                break
            myset.add(arr[j])
            mx = max(mx, arr[j])
            mn = min(mn, arr[j])
            if mx - mn == j - i:
                max_len = max(max_len, mx - mn + 1)

    return max_len

arr = [10, 12, 12, 10, 10, 11, 10]
n = len(arr)
print("Length of the longest contiguous subarray is", findLenght(arr, n))