def Nqueen(board, size, row):
    if row == size-1:
        for i in range(size):
            print(board[i])
        return True
    else:
        rownew = row + 1
        for col in range(size):
            if isvalidcell(board, size, rownew, col):
                board[rownew][col] = 1
                if Nqueen(board, size, rownew):
                    return True
                board[rownew][col] = 0
        return False


def isvalidcell(board, size, rownew, col):
    valid = True
    for i in range(rownew, -1, -1):
        if board[i][col] == 1:
            valid = False

    for i, j in zip(range(rownew, -1, -1), range(col, -1, -1)):
        if board[i][j] == 1:
            valid = False

    for i, j in zip(range(rownew, -1, -1), range(col, size)):
        if board[i][j] == 1:
            valid = False
    return valid


board = [[0] * 8 for _ in range(8)]
size = 8
Nqueen(board, size, -1)
