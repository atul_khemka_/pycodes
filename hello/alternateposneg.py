def rearrange(arr,n):
    i = n
    for j in range(n-1,0,-1):
        if arr[j] == 0:
            i -= 1
            arr[i], arr[j] = arr[j], arr[i]


arr = [1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0, 9]
n = len(arr)
rearrange(arr, n)
for i in range(n):
    print(arr[i], end=" ")


"""def rearrange(arr,n):
    i = -1
    for j in range(n):
        if arr[j] < 0 :
            i += 1
            arr[i], arr[j] = arr[j], arr[i]

    pos, neg = i + 1, 0
    while pos < n and arr[neg] < 0 and neg < pos:
        arr[neg], arr[pos] = arr[pos], arr[neg]
        pos += 1
        neg += 2

arr = [-1, 2, -3, 4, 5, 6, -7, 8, 9]
n = len(arr)
rearrange(arr, n)
for i in range(n):
    print(arr[i], end=" ")"""