# find shortest distances between every pair of vertices in a given edge weighted directed Graph
# can be used to to find negative weight cycle if distance to self becomes negative
INF = 99999
v = 4

def floydWarshall(graph):

    dist = [row[:] for row in graph]

    for k in range(v):
        for i in range(v):
            for j in range(v):
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])

    printresult(dist)

def printresult(dist):
    print("Following matrix shows the shortest distances between every pair of vertices")
    for i in range(v):
        for j in range(v):
            if dist[i][j] == INF:
                print("INF", " "*2, end=" ")
            else:
                print(dist[i][j], "\t ", end=" ")
            if j == v-1:
                print("")


graph = [[0, 5, INF, 10], [INF, 0, 3, INF], [INF, INF, 0,   1], [INF, INF, INF, 0]]
# Print the solution
floydWarshall(graph)
