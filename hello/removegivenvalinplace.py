def remove_key(arr, key):
    size = 0
    for i in range(len(arr)):
        if arr[i] != key:
            arr[size] = arr[i]
            size += 1

    return size


if __name__ == '__main__':
    arr = [1, 0, 2, 2, 3, 4, 2]
    key = 2
    size = remove_key(arr, key)
    print("input array", arr)
    print("Array after removing ", key)
    print(size)
    print(arr[:size])
