class BinaryHeap:

    def __init__(self):
        self.bheap = []

    def size(self):
        return len(self.bheap)

    def parent(self, i):
        return (i - 1)//2

    def left(self, i):
        return 2*i + 1

    def right(self, i):
        return 2*i + 2

    def getMin(self):
        return self.bheap[0]

    def extractMin(self):
        if self.size() == 0:
            return None
        min = self.getMin()
        self.bheap[0] = self.bheap[-1]
        del self.bheap[-1]
        self.minHeapify(0)
        return min

    def minHeapify(self, i):
        l = self.left(i)
        r = self.right(i)
        smallest = i
        if l < self.size() and self.bheap[l] < self.bheap[i]:
            smallest = l
        if r < self.size() and self.bheap[r] < self.bheap[smallest]:
            smallest = r
        if smallest != i:
            self.bheap[i], self.bheap[smallest] = self.bheap[smallest], self.bheap[i]
            self.minHeapify(smallest)

    def insertKey(self, key):
        i = self.size()
        self.bheap.append(key)
        while i != 0 and self.bheap[self.parent(i)] > self.bheap[i]:
            self.bheap[i], self.bheap[self.parent(i)] = self.bheap[self.parent(i)], self.bheap[i]
            i = self.parent(i)

    def deletekey(self, i):
        self.decreaseKey(i, float("-inf"))
        self.extractMin()

    def decreaseKey(self, i, new_val):
        self.bheap[i] = new_val
        while i != 0 and self.bheap[self.parent(i)] > self.bheap[i]:
            self.bheap[i], self.bheap[self.parent(i)] = self.bheap[self.parent(i)], self.bheap[i]
            i = self.parent(i)


bheap = BinaryHeap()

print('Menu')
print('insert <data>')
print('min get')
print('min extract')
print('delete key')
print('quit')

while True:
    do = input('What would you like to do? ').split()

    operation = do[0].strip().lower()
    if operation == 'i':
        data = int(do[1])
        bheap.insertKey(data)
    elif operation == 'min':
        suboperation = do[1].strip().lower()
        if suboperation == 'get':
            print('Minimum value: {}'.format(bheap.getMin()))
        elif suboperation == 'extract':
            print('Minimum value removed: {}'.format(bheap.extractMin()))
    elif operation == 'del':
        data = int(do[1])
        bheap.deletekey(data)
    elif operation == 'quit':
        break
