from collections import defaultdict


class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    def isCyclicUtil(self, node, visited, parent):
        visited[node] = True

        for i in self.graph[node]:
            if not visited[i]:
                if self.isCyclicUtil(i, visited, node):
                    return True
                # If an adjacent vertex is visited and not parent of current vertex,
                # then there is a cycle
            elif parent != i:
                return True

        return False

    def isCyclic(self):
        visited = [False] * self.V
        for i in range(self.V):
            if not visited[i]:
                return True if self.isCyclicUtil(i, visited, -1) else False


g = Graph(5)
g.add_edge(1, 0)
g.add_edge(0, 2)
g.add_edge(2, 0)
g.add_edge(0, 3)
g.add_edge(3, 4)

if g.isCyclic():
    print("Graph contains cycle")
else:
    print("Graph does not contain cycle ")
g1 = Graph(3)
g1.add_edge(0, 1)
g1.add_edge(1, 2)

if g1.isCyclic():
    print("Graph contains cycle")
else:
    print("Graph does not contain cycle ")