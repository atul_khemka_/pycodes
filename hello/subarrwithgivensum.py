def subArraySum(arr, n, Sum):
    curr_sum = 0
    hmap = {}
    for i in range(n):
        curr_sum = curr_sum + arr[i]
        if curr_sum == Sum:
            print("sub-array found between index 0 and ", i)
            return

        if curr_sum - Sum in hmap:
            print("sum found between index ", hmap[curr_sum - Sum] + 1, "to", i)
            return
        hmap[curr_sum] = i

    print("no such sub-array present")


if __name__ == "__main__":
    arr = [4, 2, -3, 1, 6]
    n = len(arr)
    Sum = 0
    subArraySum(arr, n, Sum)
