def CountTriangles(arr):
    n = len(arr)
    arr.sort()
    count = 0

    for i in range (n-1, 0, -1):
        l = 0
        r = i-1
        while r > l:
            if arr[l] + arr[r] > arr[i]:
                count += r - l
                r += -1
            else :
                l += 1

    print("no of triangles : ", count)

if __name__ == '__main__':
    A = [4, 3, 5, 7, 6];

    CountTriangles(A);