# can be used to find run time of function or log the functions args and time 

"""def f():
    print("kumar")


def f1(func):
    def wrapper():
        print("Atul")
        func()
        print("khemka")
    return wrapper

f1(f)()
#x = f1(f)
# x()"""


def f1(func):
    def wrapper(*args, **kwargs):
        print("Atul")
        val = func(*args, **kwargs)
        print("khemka")
        return val
    return wrapper


@f1
def f(x):
    print("kumar "+x)


@f1
def add(x, y):
    return x+y


f("ji")
print(add(4,7))



