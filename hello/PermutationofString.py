def permutation_of_string(inputArr, res, count, level):
    if level == len(inputArr):
        print(res)
        return
    else:
        for i in range(len(inputArr)):
            if count[i] == 0:
                continue
            else:
                res[level] = inputArr[i]
                count[i] -= 1
                permutation_of_string(inputArr, res, count, level+1)
                count[i] += 1


inputArr = ['A', 'B', 'C']
count = [1, 1, 1]
level = 0
res = ['']*3
permutation_of_string(inputArr, res, count, level)
