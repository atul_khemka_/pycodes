from collections import defaultdict


class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)

    def isCyclicUtil(self, node, visited, recstack):
        visited[node] = recstack[node] = True

        for i in self.graph[node]:
            if not visited[i]:
                if self.isCyclicUtil(i, visited, recstack):
                    return True
            elif recstack[i]:
                return True

        recstack[node] = False
        return False

    def isCyclic(self):
        visited = recstack = [False]*self.V
        for i in range(self.V):
            if not visited[i]:
                return True if self.isCyclicUtil(i, visited, recstack) else False

    def isCyclicUtil_color(self, node, color):
        color[node] = "Grey"

        for i in self.graph[node]:
            if color[i] == "Grey":
                return True
            if color[i] == "White" and self.isCyclicUtil_color(i, color):
                return True

        color[node] = "Black"
        return False

    def isCyclic_color(self):
        color = ["White"] * self.V
        for i in range(self.V):
            if color[i] == "White":
                return True if self.isCyclicUtil_color(i, color) else False


g = Graph(4)
g.add_edge(0, 1)
g.add_edge(0, 2)
g.add_edge(1, 2)
g.add_edge(2, 0)
g.add_edge(2, 3)
g.add_edge(3, 3)
if g.isCyclic_color():
    print("Graph has a cycle")
else:
    print("Graph has no cycle")