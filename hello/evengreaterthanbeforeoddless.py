def rearrangeArr(arr, n):
    evenpos = int(n/2)
    oddpos = n - evenpos
    j = oddpos -1

    temp = [None]*n
    for i in range(n):
        temp[i] = arr[i]

    temp.sort()
    for i in range(0,n,+2):
        arr[i] = temp[j]
        j -= 1

    j = oddpos
    for i in range(1,n,+2):
        arr[i] = temp[j]
        j += 1


arr = [1, 2, 3, 4, 5, 6, 7]
rearrangeArr(arr, 7)
for i in range(7):
    print(arr[i], end=" ")
