def UnionArray(arr1, arr2):
    m = arr1[-1]
    n = arr2[-1]

    ans = max(m, n)
    temparr = [0]*(ans+1)
    print(arr1[0], end=" ")
    temparr[arr1[0]] += 1
    for i in range(1, len(arr1)):
        if arr1[i] != arr1[i-1]:
            print(arr1[i], end=" ")
            temparr[arr1[i]] += 1

    for j in range(0, len(arr2)):
        if temparr[arr2[j]] == 0:
            print(arr2[j], end=" ")
            temparr[arr2[j]] += 1


# Driver Code
if __name__ == "__main__":
    arr1 = [1, 2, 2, 2, 3]
    arr2 = [2, 3, 4, 5]

    UnionArray(arr1, arr2)
