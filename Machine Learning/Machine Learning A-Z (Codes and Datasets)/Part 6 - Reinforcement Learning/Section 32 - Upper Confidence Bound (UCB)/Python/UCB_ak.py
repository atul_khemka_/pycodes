import math
import pandas as pd
import matplotlib.pyplot as plt

dataset = pd.read_csv('Ads_CTR_Optimisation.csv')

N = 1000
d = 10
ads_selected = []
num_of_selection = [0]*d
sum_of_records = [0]*d
total_reward = 0
for n in range(N):
    ad = 0
    max_upper_bound = 0
    for j in range(d):
        if num_of_selection[j] > 0:
            avg_reward = sum_of_records[j] / num_of_selection[j]
            delta_i = math.sqrt(3/2 * math.log(n+1)/num_of_selection[j])
            upper_bound = avg_reward + delta_i
        else:
            upper_bound = 1e400

        if upper_bound > max_upper_bound:
            max_upper_bound = upper_bound
            ad = j
    ads_selected.append(ad)
    num_of_selection[ad] += 1
    sum_of_records[ad] += dataset.values[n, ad]
    total_reward += dataset.values[n, ad]

plt.hist(ads_selected)
plt.title('Ads selection')
plt.xlabel('Ads')
plt.ylabel('Number of times ad selected')
plt.show()
