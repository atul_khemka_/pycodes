import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset = pd.read_csv('Position_Salaries.csv')
# separate features from dependent variable
X = dataset.iloc[:, 1:-1].values
y = dataset.iloc[:, -1].values

y = y.reshape(len(y), 1)

from sklearn.preprocessing import StandardScaler
ss_X = StandardScaler()
X = ss_X.fit_transform(X)
SS_Y = StandardScaler()
y = SS_Y.fit_transform(y)

from sklearn.svm import SVR
regressor = SVR(kernel='rbf')
regressor.fit(X, y)
print(SS_Y.inverse_transform(regressor.predict(ss_X.transform([[6.5]]))))

y = SS_Y.inverse_transform(y)
plt.scatter(ss_X.inverse_transform(X), y, color='red')
plt.plot(ss_X.inverse_transform(X), SS_Y.inverse_transform(regressor.predict(X)))
plt.title('Truth or BLUFF(SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

X_grid = np.arange(min(ss_X.inverse_transform(X)), max(ss_X.inverse_transform(X)), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(ss_X.inverse_transform(X), y, color='red')
plt.plot(X_grid, SS_Y.inverse_transform(regressor.predict(ss_X.transform(X_grid))), color = 'blue')
plt.title('Truth or Bluff (Polynomial Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()


