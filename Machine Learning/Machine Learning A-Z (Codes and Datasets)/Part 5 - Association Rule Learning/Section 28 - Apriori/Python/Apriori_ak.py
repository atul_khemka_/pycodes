import pandas as pd

dataset = pd.read_csv('Market_Basket_Optimisation.csv', header=None)
basket = []
for i in range(7501):
    basket.append([str(dataset.values[i, j]) for j in range(20)])

from apyori import apriori
rules = apriori(basket, min_support=0.003, min_confidence=0.2, min_lift=3, min_length=2, max_length=2)

results = list(rules)


def inspect(results):
    lhs = [tuple(result[2][0][0])[0] for result in results]
    rhs = [tuple(result[2][0][1])[0] for result in results]
    support = [result[1] for result in results]
    confidence = [result[2][0][2] for result in results]
    lift = [result[2][0][3] for result in results]
    return list(zip(lhs,rhs,support,confidence,lift))


resultinDF = pd.DataFrame(inspect(results), columns=['left hand side', 'Right hand side', 'Support', 'confidence', 'Lift'])
print(resultinDF)

