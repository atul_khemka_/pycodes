from src.Chess import Chess
from src.Pawn import Pawn


class Game:

    def play_game(self, chess):
        while True:
            x = int(input("enter item to move : x : ")) - 1
            y = int(input("enter item to move : y : ")) - 1
            if chess.board[x][y].name != "Blank":
                x_new = int(input("enter new position : x : ")) - 1
                y_new = int(input("enter new position : y : ")) - 1
                if chess.board[x][y].player == "B":
                    x_new = 7 - x_new

                if chess.board[x][y].isvalid_move(x_new, y_new,chess.board):
                    print("good")
                    return False
                else:
                    print("fail")


if __name__=="__main__":
    chess = Chess()
    chess.initialize(Pawn)
    chess.print_board()
    game = Game()
    game.play_game(chess)
    chess.print_board()

