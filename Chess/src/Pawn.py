from src.Chess import Chess
from src.Blank import Blank


class Pawn(Chess):
    mov_path_x = [1, 1]
    mov_path_y = [-1, 1]
    name = "Pawn"

    def __init__(self, player, position):
        self.player = player
        self.position = position

    def isvalid_move(self, x_new, y_new,board):
        for i in range(len(self.mov_path_x)):
            if self.player == 'W':
                x = self.position[0] + self.mov_path_x[i]
                y = self.position[1] + self.mov_path_y[i]
                if x_new == x and y_new == y:
                    if self.board[x_new][y_new].name == blank.name:
                        return False
                    else:
                        print("player removed the opponents ",self.board[x_new][y_new].name)
                        del self.board[x_new][y_new]
                        self.position = [x_new,y_new]
                        self.board[x_new][y_new] = self
                        return True
        if x_new == self.position[0] + 1 and y_new == self.position[0]:
            if board[x_new][y_new].name == Blank.name:
                board[x_new][y_new], board[self.position[0]][self.position[1]] = board[self.position[0]][
                                                                                               self.position[1]], \
                                                                                           board[x_new][y_new]
                self.position = [x_new, y_new]
                return True
        return False
