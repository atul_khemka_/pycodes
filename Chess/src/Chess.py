from src.Blank import Blank


class Chess:
    def __init__(self):
        self.board = [[Blank()] * 8 for _ in range(8)]

    def initialize(self,pawn):
        for i in range(8):
            self.board[1][i] = pawn("W", [1, i])

        for i in range(8):
            self.board[6][i] = pawn("B", [6, i])

    def print_board(self):
        for x in range(8):
            for y in range(8):
                print(self.board[x][y].name, end=" ")
            print(" ")




